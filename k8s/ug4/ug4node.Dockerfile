FROM ubuntu:latest@sha256:aa6c2c047467afc828e77e306041b7fa4a65734fe3449a54aa9c280822b0d87d as ug4node

ARG USER=mpiuser
ARG SSH_PATH=/etc/ssh

#ugnode specific args
ARG BUILD_DEPENDENCIES="build-essential cmake git python3"
ARG UG4_CONF_DIM="2;3"
ARG UG4_CONF_CPU="1"
ARG UG4_PARALLEL="ON"
ARG UG4_CONF_DIFF="ON"
ARG WITH_XEUS=OFF

RUN apt-get update \ 
    && apt-get -y install \
    bash \
    openmpi-bin libopenmpi-dev\
    openssh-server openssh-client\
    procps \
    nfs-common \
    neovim \
    tini \
    curl \
    dnsutils \
    ${BUILD_DEPENDENCIES}

RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# USER mpiuser
WORKDIR /opt
ENV PATH=$PATH:/opt/ughub
ENV UG4_ROOT=/opt/ug4-dev
RUN git clone https://github.com/UG4/ughub ughub\
    && mkdir -p /opt/ug4-dev \
    && cd /opt/ug4-dev \
    && ughub init \
    && ughub list \
    && ughub install Examples

RUN echo "Setting up the build directory" \ 
    && mkdir -p /opt/ug4-dev/build \
    && cd /opt/ug4-dev/build \ 
    && cmake -DPROFILER=Shiny .. -DCMAKE_CXX_COMPILER=mpicxx -DCMAKE_C_COMPILER=mpicc -DDIM=${UG4_CONF_DIM} -DENABLE_ALL_PLUGINS=OFF -DCPU=${UG4_CONF_CPU} -DCOMPILE_INFO=OFF -DUSE_XEUS=${WITH_XEUS} \
    && make -j8

RUN echo "Setting up the build directory" \ 
    && cd /opt/ug4-dev/build \ 
    && cmake -DConvectionDiffusion=${UG4_CONF_DIFF} .. \
    && make -j8


RUN adduser --gecos "MPI Test User" --shell /bin/bash --disabled-password ${USER} \
    && echo "${USER} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers \
    && echo ${USER}:* | chpasswd \
    && echo root:* | chpasswd

RUN mkdir ~/cloud

WORKDIR /
