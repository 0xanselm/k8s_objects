FROM ubuntu:latest@sha256:aa6c2c047467afc828e77e306041b7fa4a65734fe3449a54aa9c280822b0d87d as osu-bench

ARG USER=osu
ARG SSH_PATH=/etc/ssh

RUN apt-get update \ 
    && apt-get -y install \
    bash \
    openmpi-bin libopenmpi-dev\
    openssh-server openssh-client\
    procps \
    nfs-common \
    neovim \
    tini \
    curl \
    dnsutils \
    iputils-tracepath \
    iputils-ping \
    iputils-clockdiff \
    build-essential \
    cmake \
    wget \
    python3 \
    git

RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir ~/cloud

WORKDIR /
RUN wget http://mvapich.cse.ohio-state.edu/download/mvapich/osu-micro-benchmarks-5.6.2.tar.gz \
    && tar zxvf ./osu-micro-benchmarks-5.6.2.tar.gz \
    && rm osu-micro-benchmarks-5.6.2.tar.gz
WORKDIR /osu-micro-benchmarks-5.6.2
RUN which mpicc && ./configure CC=/usr/bin/mpicc CXX=/usr/bin/mpicxx
RUN make -j 4 && make install

WORKDIR /
RUN git clone https://github.com/Mantevo/miniAMR.git /miniAMR \
    && cd /miniAMR/ref \
    && make
