FROM amd64/alpine@sha256:a777c9c66ba177ccfea23f2a216ff6721e78a662cd17019488c417135299cd89 as node

ARG USER=mpiuser
ARG SSH_PATH=/etc/ssh
RUN ping -c 2 8.8.8.8

RUN apk add --no-cache \
    bash \
    build-base \
    libc6-compat \
    openmpi openmpi-dev\
    openssh \
    openrc \
    nfs-utils \
    neovim \
    tini

RUN rm -rf /var/cache/apk

#https://wiki.alpinelinux.org/wiki/Setting_up_a_nfs-server
#https://wiki.alpinelinux.org/wiki/Setting_up_a_SSH_server

RUN adduser -S ${USER} -g "MPI Test User" -s /bin/ash -D ${USER} \
    && echo "${USER} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers \
    && echo ${USER}:* | chpasswd \
    && echo root:* | chpasswd

RUN mkdir ~/.ssh \
    # && rc-update add sshd \
    # && rc-status \
    # touch softlevel because system was initialized without openrc
    && echo "PermitRootLogin yes" >> ${SSH_PATH}/sshd_config \
    && echo "PubkeyAuthentication yes" >> ${SSH_PATH}/sshd_config \
    && echo "StrictHostKeyChecking no"  >> ${SSH_PATH}/ssh_config \
    && rm /etc/motd

# COPY --chmod=770 ./node_script/helper_node.sh /root/

RUN mkdir ~/cloud

# Using tini - All Tini does is spawn a single child (Tini is meant to be run in a container), and wait for it to exit all the while reaping zombies and performing signal forwarding.
# Docu: https://github.com/krallin/tini

# ENTRYPOINT ["/sbin/tini", "-g", "-e 143" ,"-e 137", "--", "/root/helper_node.sh"]

