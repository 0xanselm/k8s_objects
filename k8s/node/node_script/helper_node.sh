#!/bin/sh

# 1. Set up ssh/sshd
# 2. Write ip hostname >> /etc/hosts  hostname -i >> $LOG_FILE
# 3. Mount nfs vol given NFS server is online
# 4. Handling of trap signals [SIG]
# 5. Clean up for next run

HOST_FILE="/root/cloud/hosts"
LOG_FILE="/root/cloud/log.test"
NFS_SERVER_IP="/root/cloud/serv.ip"
COMP_OUT_FILE="/root/cloud/test" #compilation out from last run

echo "I am Containered. Help"
echo "Running as: $(whoami):$(id -gn)"

# 1.
# Client side
touch ~/.ssh/authorized_keys
cat ~/.ssh/id_rsa.pub > ~/.ssh/authorized_keys
chown root -R ~/.ssh
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys
chmod 600 ~/.ssh/id_rsa*

# 2. 
echo "$(hostname -i) $(hostname)" >> "${HOST_FILE}"


# Define cleanup procedure
# https://stackoverflow.com/questions/41451159/how-to-execute-a-script-when-i-terminate-a-docker-container
function cleanupQUIT() {
    echo "$(hostname -i) $(date +%H:%M:%S_%j) [SIGQUIT] Container stopped, performing cleanup. $(date +%H:%M:%S_%j)" >> "${LOG_FILE}"
    echo got SIGQUIT
}

function cleanupTERM {
    echo "$(hostname -i) $(date +%H:%M:%S_%j) [SIGTERM] Container stopped, performing cleanup" >> "${LOG_FILE}"
    echo got SIGTERM
}

function cleanupINT {
    echo "$(hostname -i) $(date +%H:%M:%S_%j) [SIGINT] Container stopped, performing cleanup." >> "${LOG_FILE}"
    echo got SIGINT
}

function cleanupEXIT {
    echo "$(hostname -i) $(date +%H:%M:%S_%j) [EXIT] sshd restarted." >> "${LOG_FILE}"
    # /usr/sbin/sshd -D -d -h /root/.ssh/id_rsa -f /etc/ssh/sshd_config
    echo got EXIT
}

function startLOG() {
    echo "$(hostname -i) $(date +%H:%M:%S_%j) [STARTED] Container started. " >> "${LOG_FILE}"
}

startLOG
trap cleanupEXIT EXIT
trap cleanupINT SIGINT
trap cleanupTERM SIGTERM
trap cleanupQUIT SIGQUIT

# mount NFS server file
# mount -v -o vers=4,loud nfsserv:/ /root/cloud
COUNTER=0
while [ ! -f "${NFS_SERVER_IP}" ] || [ "${COUNTER}" -lt 1 ];  
do
    sleep 1 # or less like 0.2
    echo "NFS Server at: cat "${NFS_SERVER_IP}""  
    mount -v "$(cat ${NFS_SERVER_IP})":/ ~/cloud
    df -h -T -t nfs4
    let COUNTER+=1
done

# Start sshd i.e. ssh server but gracefully make it shout up
/usr/sbin/sshd -D -d -h /root/.ssh/id_rsa -f /etc/ssh/sshd_config > /dev/null 2>&1

# Clean up HOST_FILE s.t. execution of mpi is no longer possible and results in a clean env for next run
>"${HOST_FILE}"
rm "${COMP_OUT_FILE}"
# Execute a command
# exec "$@"
"${@}" &