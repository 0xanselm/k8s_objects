# Setting up the server
# https://hub.docker.com/r/itsthenetwork/nfs-server-alpine/
FROM amd64/alpine@sha256:a777c9c66ba177ccfea23f2a216ff6721e78a662cd17019488c417135299cd89 as node

ARG USER=mpiuser
ARG SSH_PATH=/etc/ssh
RUN ping -c 2 8.8.8.8

RUN apk add --no-cache --update --verbose nfs-utils bash iproute2 && \
    rm -rf /var/cache/apk /tmp /sbin/halt /sbin/poweroff /sbin/reboot && \
    mkdir -p /var/lib/nfs/rpc_pipefs /var/lib/nfs/v4recovery && \
    echo "rpc_pipefs    /var/lib/nfs/rpc_pipefs rpc_pipefs      defaults        0       0" >> /etc/fstab && \
    echo "nfsd  /proc/fs/nfsd   nfsd    defaults        0       0" >> /etc/fstab

# Copy skel_exports to /etc
COPY ./nfs_script/skel_exports /etc/

# When using NFSv4, make sure TCP port 2049 is open. No other port opening should be required
# RUN echo "-A INPUT -p tcp -m tcp --dport 2049 -j ACCEPT" >> /etc/iptables/iptables.rules

# COPY --chmod=770 ./nfs_script/helper_nfsd.sh /root/

RUN mkdir ~/cloud \
    && cat /etc/skel_exports >> /etc/exports 

# ENTRYPOINT ["/root/helper_nfsd.sh"]