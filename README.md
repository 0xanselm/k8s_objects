# My Kubernetes cluster notes
**Please see my notes to create a multi node Kubernetes cluster for HPC applications on bare-metal.**
![My bare-metal k8s cluster overview.](k8s/kubernetes_setup.png)


## Steps for creating a cluster with kubeadm
`init cluster -> install pod network addon [-> join more control-plane nodes] -> join worker nodes [-> upgrade cluster]`
#### Preparing all hosts: 
- Install a container runtime interface (like cri-o) into each node in the cluster so that Pods can run there
- Install cri-o
    - Install: https://github.com/cri-o/cri-o/blob/main/install.md
    - Arch specific instructions: https://wiki.archlinux.org/title/CRI-O
    - Install plugins documented here https://github.com/cri-o/cri-o/blob/main/contrib/cni/README.md and `cp` to `opt/cni/bin`
- Install kubeadm, kubectl and kubelet on each node.
    - Check that kubectl runs correctly, i.e. make sure all environment flags are correct. Check in `/etc/kubernetes/kubelet.env` that the `cni-bin-dir` is at commented out, since for `kubelet version` >= 1.24 this setting is missing
- Install the cni-plugins 
    - In case of flannel, dont forget flannel cni-plugin from here: https://github.com/flannel-io/cni-plugin
    - In case of calcio, look here: https://projectcalico.docs.tigera.io/getting-started/kubernetes/hardway/install-cni-plugin
- Check with `lsmod` that `br_netfilter` and `overlay` is loaded in the kernel, otherwise `modprobe br_netfilter && depmod -a` or `modprobe overlay && depmod -a`
- Check firewall settings, best switch off if possible: https://stackoverflow.com/questions/67328141/cant-resolve-dns-in-kubernetes
- Check and clean `iptables`: `sudo iptables -F && sudo iptables -t nat -F && sudo iptables -t mangle -F && sudo iptables -X && sudo reboot`
- Disable swap `swapoff -a`
- Make sure the right cgroup driver `systemd` is defined in `crio.conf`
- Enable service: `systemctl enable crio`
- Enable service: `systemctl enable kubelet`
- Make sure `$HOME/.kube` is empty
- Make sure `/etc/cni/net.d/` is sufficiently ordered (i.e. 10-***.conf exists only once)
- Tip: reboot

#### Initializing your control-plane node with
- Clean `/etc/cni/net.d/` s.t. `10-flannel.conflist` is not deleted from prev cluster
- Run `# kubeadm init --config kubeadm-config.yaml`. Output _like_ so 
    ```
    Your Kubernetes control-plane has initialized successfully!

    To start using your cluster, you need to run the following as a regular user:

    mkdir -p $HOME/.kube
    sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    sudo chown $(id -u):$(id -g) $HOME/.kube/config

    Alternatively, if you are the root user, you can run:

    export KUBECONFIG=/etc/kubernetes/admin.conf

    You should now deploy a pod network to the cluster.
    Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
    https://kubernetes.io/docs/concepts/cluster-administration/addons/

    Then you can join any number of worker nodes by running the following on each as root:

    kubeadm join 192.168.1.170:6443 --token 4wpkba.7ijdgak3bqvuubss \
        --discovery-token-ca-cert-hash sha256:98a4613b54a9929ad6c45575fb5a1bd2b722a14d2f60ddced1790af06edd70c1 

     ```
- Optionally untaint control-pane:
    `kubectl taint nodes --all node-role.kubernetes.io/master-`
    or `kubectl taint nodes --all node-role.kubernetes.io/control-plane- node-role.kubernetes.io/master-`
- Installing a Pod network add-on.
    - Flannel with `kubectl apply -f kube-flannel.yaml`
- Ready now
- Monitor cluster `watch kubectl get deployments,pods,svc,configmaps -A -o wide`
- Reprint join command `kubeadm token create --print-join-command`
- VNode ConfigMap `kubectl apply -f vnode-config.yaml'`
- SSH ConfigMap `kubectl apply -f ssh-config.yaml`


### Shutdown cluster
`kubectl drain <node name> --delete-emptydir-data --ignore-daemonsets`
`kubectl delete node <node name>`
`kubeadm reset`
`sudo ip link delete cni0 && sudo ip link delete flannel.1`

### `kubeadm-config.yaml`
```
cat kubeadm-config.yaml         
apiVersion: kubeadm.k8s.io/v1beta3
kind: InitConfiguration
nodeRegistration:
  criSocket: "unix:///var/run/crio/crio.sock"
---
apiVersion: kubeadm.k8s.io/v1beta3
kind: ClusterConfiguration
kubernetesVersion: v1.24.0
networking:
  # podSubnet: "192.168.0.0/16" # Calico
  podSubnet: "10.244.0.0/16" # flannel 
---
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
cgroupDriver: systemd
```
