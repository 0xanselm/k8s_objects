#!/bin/bash
# ./mytest.sh | tee myResult.txt
tracepath $(tail -n 1 $HOME/cloud/hosts) \
&& ping -c 30 $(tail -n 1 $HOME/cloud/hosts) \
&& mpirun -np 2 --mca opal_warn_on_missing_libcuda 0 --map-by node --hostfile $HOME/cloud/hosts ./osu-micro-benchmarks-5.6.2/mpi/pt2pt/osu_bw \
&& mpirun -np 2 --mca opal_warn_on_missing_libcuda 0 --map-by node --hostfile $HOME/cloud/hosts ./osu-micro-benchmarks-5.6.2/mpi/pt2pt/osu_bibw \
&& mpirun -np 2 --mca opal_warn_on_missing_libcuda 0 --map-by node --hostfile $HOME/cloud/hosts ./osu-micro-benchmarks-5.6.2/mpi/pt2pt/osu_multi_lat \
&& mpirun -np 2 --mca opal_warn_on_missing_libcuda 0 --map-by node --hostfile $HOME/cloud/hosts ./osu-micro-benchmarks-5.6.2/mpi/collective/osu_iallgatherv \
&& mpirun -np 2 --mca opal_warn_on_missing_libcuda 0 --map-by node --hostfile $HOME/cloud/hosts ./osu-micro-benchmarks-5.6.2/mpi/collective/osu_iscatterv \
&& mpirun -np 2 --mca opal_warn_on_missing_libcuda 0 --map-by node --hostfile $HOME/cloud/hosts ./osu-micro-benchmarks-5.6.2/mpi/collective/osu_alltoallv \
&& mpirun -np 2 --mca opal_warn_on_missing_libcuda 0 --map-by node --hostfile $HOME/cloud/hosts ./osu-micro-benchmarks-5.6.2/mpi/collective/osu_allreduce \
&& mpirun -np 2 --mca opal_warn_on_missing_libcuda 0 --map-by node --hostfile $HOME/cloud/hosts ./osu-micro-benchmarks-5.6.2/mpi/collective/osu_ialltoall \
&& mpirun -np 2 --mca opal_warn_on_missing_libcuda 0 --map-by node --hostfile $HOME/cloud/hosts ./osu-micro-benchmarks-5.6.2/mpi/collective/osu_ibcast \
&& mpirun -np 2 --mca opal_warn_on_missing_libcuda 0 --map-by node --hostfile $HOME/cloud/hosts ./osu-micro-benchmarks-5.6.2/mpi/collective/osu_igatherv \
&& mpirun -np 2 --mca opal_warn_on_missing_libcuda 0 --map-by node --hostfile $HOME/cloud/hosts ./osu-micro-benchmarks-5.6.2/mpi/collective/osu_ireduce \
&& mpirun -np 2 --mca opal_warn_on_missing_libcuda 0 --map-by node --hostfile $HOME/cloud/hosts ./osu-micro-benchmarks-5.6.2/mpi/collective/osu_iscatterv \
&& mpirun -np 2 --mca opal_warn_on_missing_libcuda 0 --map-by node --hostfile $HOME/cloud/hosts ./osu-micro-benchmarks-5.6.2/mpi/collective/osu_scatterv \
&& mpirun -np 2 --mca opal_warn_on_missing_libcuda 0 --map-by node --hostfile $HOME/cloud/hosts ./osu-micro-benchmarks-5.6.2/mpi/collective/osu_gatherv
